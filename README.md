# p5.gui-builder
p5.gui-builder is a simple to use tool to create 2D GUIs for projects using [p5.js](http://p5js.org).
Drawing in 3D unsupported but you can always use a separate graphics buffer and draw the buffer in 2D.
This is currently a work in progress. Nothing is final and almost nothing is completely tested.

## Usage
Import all files and create an instance of GUI. Elements are added afterwards with a reference to the parent.
If you want to persist what you've drawn it is necessary to use a separate graphics buffer.

The library is based around simple elements, inheritance and an event system.

## List of predefined events (and the arguments passed when triggered)
* click ()
* drag_start ()
* drag (delta_x, delta_y)
* drag_end ()

## Issues
The library is still in development and issues are to be expected for now.

## Todo
* add more elements: labels, sliders and groups to automatically align all children
* even more elements for later: dropdowns, radio buttons, checkboxes and dials
* add the ability to animate elements
* add a function to make a parent just big enough to fit the children and vice versa
* provide more detail on how to use the library
* use the functions given by p5 to make usage easier (e.g. automatic binding to the p5 events), see https://github.com/processing/p5.js/wiki/Libraries for details.
* catch interactions with elements (means don't let the event propagate to the normal p5 event if it is handled by the GUI)
* pass a function as value for an element and evaluate it when drawing the element
* configure CI to create a single file version and a minified version
* add a proper documentation to the elements (use yuidoc)
* test if it works in instance mode as well

## Contributors
* [Tom Winter](http://gitlab.com/twinter)

## License
p5.gui-builder
Copyright (C) 2017, see README.md for a list of the contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
