/*
 * p5.gui-builder
 * Copyright (C) 2017, see README.md for a list of the contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

function AbstractGUIElement(parent, pos_x, pos_y, size_x, size_y) {
	/**
	 * This is the basis for every element of the UI.
	 * The whole UI is heavily based on inheritance and positioning is
	 *   relative to the parent.
	*/

	// position defines the top left corner of the hitbox
	if (typeof(pos_y) === 'undefined') {
		this._position_x = 0;
		this._position_y = 0;
	} else {
		this._position_x = pos_x;
		this._position_y = pos_y;
	}

	// the size of the hitbox
	if (typeof(size_y) === 'undefined') {
		this._size_x = 25;
		this._size_y = 25;
	} else {
		this._size_x = size_x;
		this._size_y = size_y;
	}

	// format of every element in this.children: and this.children_hidden
    //   {object: [x1, y1, x2, y2]}
    //            |---hitbox---|
	this.children = [];
	this.children_hidden = [];

	/**
	 * Determines if the element can be dragged.
	 * Do not change this on the root element!
	 * The code to implement this is in the root element (gui.js/GUI).
	 *
	 * true:  can be dragged
	 * false: can not be dragged
	 * null:  let the event bubble up to the parent
	*/
	this.draggable = null;

	this._min_size_x = 25;
	this._min_size_y = 25;
	this._layer = 0;
	this._events = new Map();

	// The parent of this element. The parent of the root is null.
	// This isn't at the top as add_child() may require this._events (see ToggleGroup)
	this.parent = parent;
	if (parent !== null) {
		parent.add_child(this);
	}

	// create a separate graphics buffer for the gui
	if (parent === null) {
		this.graphics = createGraphics(size_x, size_y, P2D);
	} else {
		this.graphics = this.parent.graphics;
	}
};
AbstractGUIElement.prototype.get_position = function() {
	return [this._position_x, this._position_y];
};
AbstractGUIElement.prototype.set_position = function(x, y) {
	/**
	 * Sets the position relative to the parent!
	*/

	// ensure the coordinates are bigger than 0
	x = (x >= 0) ? x : 0;
	y = (y >= 0) ? y : 0;

	if (this.parent === null) {
		console.warn('the root element can not be moved.');
		return;
	}

	this._position_x = x;
	this._position_y = y;

	this._check_size();

	this._require_redraw();
};
AbstractGUIElement.prototype.get_size = function() {
	return [this._size_x, this._size_y];
};
AbstractGUIElement.prototype.set_size = function(size_x, size_y, size_checked) {
	this._size_x = size_x;
	this._size_y = size_y;

	if (size_checked !== true) {
		this._check_size(true);
	}

	for (let c of this.children) {
		c._check_size();
	}

	this._require_redraw();
};
AbstractGUIElement.prototype.get_min_size = function() {
	return [this._min_size_x, this._min_size_y];
};
AbstractGUIElement.prototype.set_min_size = function(x, y) {
	/**
	 * Can be called with 1 or 2 arguments. If only 1 argument is used it will
	 *    be apllied to both the x and y axes.
	*/

	// set new minimum size
	this._min_size_x = x;
	if (arguments.length > 1) {
		this._min_size_y = y;
	} else {
		this._min_size_y = x;
	}

	// ensure the actual size is at least the new minimum size
	this._check_size();
};
AbstractGUIElement.prototype._check_size = function(direct_apply) {
	/**
	* Move first and scale after that.
	* Buffer changes to this._size but not for changes to this._position.
	*/

	// _check_size() is not usable on the root element
	if (this.parent === null) {
		return;
	}

	var sx = this._size_x;
	var sy = this._size_y;
	var done_x = false
	var done_y = false;

	// check if it still is completely inside of the parent
	// x axis
	if (sx > this.parent._size_x - this._position_x) {
		this._position_x = this.parent._size_x - sx;
		if (this._position_x < 0) {
			done_x = true;
			this._position_x = 0;
		}
	}
	// y axis
	if (sy > this.parent._size_y - this._position_y) {
		this._position_y = this.parent._size_y - sy;
		if (this._position_y < 0) {
			done_y = true;
			this._position_y = 0;
		}
	}

	// check minimum size
	// x axis
	if (!done_x && sx < this._min_size_x) {
		if (this.parent._size_x <= this._min_size_x) {
			this._position_x = 0;
			sx = this.parent._size_x;
		} else {
			var remaining = this.parent._size_x - sx;
			this._position_x += (this._min_size_x - sx) * this._position_x / remaining;
		}
	}
	// y axis
	if (!done_y && sy < this._min_size_y) {
		if (this.parent._size_y <= this._min_size_y) {
			this._position_y = 0;
			sy = this.parent._size_y;
		} else {
			var remaining = this.parent._size_y - sy;
			this._position_y += (this._min_size_y - sy) * this._position_y / remaining;
		}
	}

	if (sx != this._size_x || sy != this._size_y) {
		if (direct_apply === true) {
			this._size_x = sx;
			this._size_y = sy;
		} else {
			this.set_size(sx, sy, true);
		}
	}
};
AbstractGUIElement.prototype._require_redraw = function() {
	var root = this;
	while (root.parent !== null) {
		root = root.parent;
	}
	root._redraw = true;
};
AbstractGUIElement.prototype.draw = function(offset_x, offset_y) {
	/**
	 * Draws itself and all of its children.
	 * Should only be called on the root element and never with arguments.
	*/

	if (typeof(offset_y) === 'undefined') {
		offset_x = 0;
		offset_y = 0;
	}

	this.graphics.push();
	this.graphics.translate(offset_x, offset_y);

	// draw the element itself before drawing the children
	this._draw();

	for (let i = 0; i < this.children.length; i++) {
		this.children[i].draw(this._position_x, this._position_y);
	}

	this.graphics.pop();
};
AbstractGUIElement.prototype._draw = function() {
	// to be implemented by children
};
AbstractGUIElement.prototype.hide = function () {
	if (this.parent !== null) {
		this.parent._hide(this);
	} else {
		console.warn('the root of the GUI can not be hidden');
	}
};
AbstractGUIElement.prototype._hide = function(object) {
	var index = this.children.indexOf(object);
	this.children.splice(index, 1);
	this.children_hidden.push(object);

	this._require_redraw();
};
AbstractGUIElement.prototype.show = function () {
	if (this.parent !== null) {
		this.parent._show(this);
	} else {
		console.warn('the root of the GUI can not be hidden');
	}
};
AbstractGUIElement.prototype._show = function(object) {
	var index = this.children_hidden.indexOf(object);
	this.children_hidden.splice(index, 1);
	this.add_child(object);

	this._require_redraw();
};
AbstractGUIElement.prototype.is_visible = function () {
	if (this.parent !== null) {
		return this.parent._is_visible(this);
	} else {
		return true;
	}
};
AbstractGUIElement.prototype._is_visible = function(object) {
	if (this.children.includes(object)) {
		return true;
	} else if (this.children_hidden.includes(object)) {
		return false;
	} else {
		throw TypeError('object is not a child of UI');
	}
};
AbstractGUIElement.prototype.toggle_visibility = function(object) {
	if (this.is_visible(object)) {
		this.hide(object);
	} else {
		this.show(object);
	}
};
AbstractGUIElement.prototype.set_layer = function(layer) {
	this._layer = layer;
	if (this.parent !== null) {
		if (this.is_visible) {
			this.parent._sort_children();
			this._require_redraw();
		}
	} else {
		console.log('setting the layer of the root doesn\'t really make sense.');
	}
};
AbstractGUIElement.prototype._sort_children = function() {
	this.children.sort(
		function(a, b) {
			return a._layer - b._layer;
		}
	);
};
AbstractGUIElement.prototype.add_child = function(object, hidden) {
	if (hidden !== true) {
		var i = 0;
		while (i < this.children.length && this.children[i]._layer <= object._layer) {
			i++;
		}
		this.children.splice(i, 0, object);
		object._check_size();
		this._require_redraw();
	} else {
		this.children_hidden.push(object);
	}
	object.parent = this;
};
AbstractGUIElement.prototype.remove_child = function(object) {
	var index = this.children.indexOf(object);
	if (index > -1) {
		this.children.splice(index, 1);
		object.parent = null;
		this._require_redraw();
		return;
	}

	index = this.children_hidden.indexOf(object);
	if (index > -1) {
		this.children_hidden.splice(index, 1);
		object.parent = null;
		return;
	}

	throw new Error('object is not a child');
};
AbstractGUIElement.prototype.get_children = function(visible) {
	/**
	 * visible can be either undefined, true or false. It returns the following:
	 * undefined: visible and hidden children of this element
	 * true:      only the visible children
	 * false:     only the hidden children
	*/

	if (typeof(visible) === 'undefined') {
		return this.children.concat(this.children_hidden);
	} else if (visible === true) {
		return this.children;
	} else if (visible === false) {
		return this.children_hidden;
	}
	throw TypeError('parameter has to be undefined, true or false');
};
AbstractGUIElement.prototype.clear_children = function() {
	for (let c of this.children) {
		c.parent = null;
	}
	this.children = [];
	for (let c of this.children_hidden) {
		c.parent = null;
	}
	this.children_hidden = [];

	this._require_redraw();
};
AbstractGUIElement.prototype.check_hit = function(x, y, event_name) {
	/**
	 * Return the topmost element on x, y.
	 * Never set called_recursive manually (and especially not to true)!
	*/

	// aplly the offset
	x -= this._position_x;
	y -= this._position_y;

	// hit detection in reverse order to check the topmost element first
	for (let i = this.children.length-1; i >= 0; i--) {
		// check hitboxes
		if (x >= this.children[i]._position_x
			&& y >= this.children[i]._position_y
			&& x <= this.children[i]._position_x + this.children[i]._size_x
			&& y <= this.children[i]._position_y + this.children[i]._size_y) {
			// check children
			var r = this.children[i].check_hit(x, y, event_name);

			if (r !== false) {
				return r;
			}
		}
	}

	// no hit among the children or no children
	// does the event even exist on this element?
	if (event_name !== undefined && this.has_event(event_name) === false) {
		return false;
	}

	// is it really a hit on the element and not just the hitbox?
	if (this._check_hit_precise(x, y) === false) {
		return false;
	}

	return this;
};
AbstractGUIElement.prototype._check_hit_precise = function(x, y) {
	/**
	 * Does the precise hit detection and is therefore specific to the shape of
	 *   the element.
	 * Has to be overwritten by children that are not rectanglar.
	 *
	 * The coordinates are relative to the element itself.
	 * This means that the top left corner of the hitbox is at (0, 0).
	*/
	if (x >= 0 && y >= 0
		&& x <= this._size_x && y <= this._size_y) {
		return true;
	}
	return false;
};
AbstractGUIElement.prototype.bind_event = function(name, callable, this_arg, ...args) {
	/**
	 * name:      the name of the event (e.g. 'click' for mouseclicks)
	 * callable:  the function to execute
	 * this_args: the context to execute the function in
	 * [args]:    array of arguments to pass on to the function on execution
	*/

	var e = this._events.get(name);
	if (e === undefined) {
		e = [];
	}
	e.push([callable, this_arg, args]);
	this._events.set(name, e);
};
AbstractGUIElement.prototype.clear_event = function(name) {
	/**
	 * Unbinds ALL functions bound to the named event.
	*/

	this._events.delete(name);
};
AbstractGUIElement.prototype.check_event = function(x, y, name, ...args) {
	var o = this.check_hit(x, y, name);
	if (o !== false) {
		o.trigger_event(name, args);
	}
};
AbstractGUIElement.prototype.has_event = function(name) {
	return this._events.has(name);
};
AbstractGUIElement.prototype.trigger_event = function(name, ...args) {
	var e = this._events.get(name);
	if (e === undefined) {
		return false;
	}

	for (let c of e) {
		c[0].apply(c[1], c[2].concat(args));
	}
};
