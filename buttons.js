/*
 * p5.gui-builder
 * Copyright (C) 2017, see README.md for a list of the contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

function AbstractButton(parent, label, x, y, size_x, size_y) {
	AbstractGUIElement.call(this, parent, x, y, size_x, size_y);
    this.label = label;
    this.fill = color(255);
    this.stroke = color(0);
    this.stroke_weight = 1;
}
AbstractButton.prototype = Object.create(AbstractGUIElement.prototype);
AbstractButton.prototype.constructor = AbstractButton;




function ButtonRound(parent, label, x, y, d) {
	// TODO: change this to also support ellipses
    AbstractButton.call(this, parent, label, x, y);
    this._size_x = d;
	this._size_y = d;
};
ButtonRound.prototype = Object.create(AbstractButton.prototype);
ButtonRound.prototype.constructor = ButtonRound;
ButtonRound.prototype.set_diameter = function(d) {
	this._size_x = d;
	this._size_y = d;
};
ButtonRound.prototype._check_hit_precise = function(x, y) { // XXX: bugfix! pos.x = 0!
	var r =  this._size_x / 2
    var dx = x - r;
    var dy = y - r;
    if (Math.sqrt(dx * dx + dy * dy) <= r) {
        return true;
    }
    return false;
};
ButtonRound.prototype._draw = function() {
    this.graphics.fill(this.fill);
    this.graphics.stroke(this.stroke);
    this.graphics.strokeWeight(this.stroke_weight);
    this.graphics.ellipseMode(CORNER);
    this.graphics.ellipse(this._position_x, this._position_y, this._size_x, this._size_y);

    this.graphics.fill(0);
    this.graphics.noStroke();
    this.graphics.textAlign(CENTER, CENTER);
    this.graphics.text(this.label, this._position_x + this._size_x / 2, this._position_y + this._size_y / 2);
};




function ButtonRect(parent, label, x, y, size_x, size_y) {
	AbstractButton.call(this, parent, label, x, y, size_x, size_y);
};
ButtonRect.prototype = Object.create(AbstractButton.prototype);
ButtonRect.prototype.constructor = ButtonRect;
ButtonRect.prototype._draw = function() {
	this.graphics.fill(this.fill);
	this.graphics.stroke(this.stroke);
	this.graphics.strokeWeight(this.stroke_weight);
	this.graphics.rectMode(CORNER);
	this.graphics.rect(this._position_x, this._position_y, this._size_x, this._size_y);

	this.graphics.fill(0);
	this.graphics.noStroke();
	this.graphics.textAlign(CENTER, CENTER);
	this.graphics.text(this.label, this._position_x + this._size_x / 2, this._position_y + this._size_y / 2);
};
