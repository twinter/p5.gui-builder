/*
 * p5.gui-builder
 * Copyright (C) 2017, see README.md for a list of the contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

function ToggleGroup(parent, x, y, size_x, size_y) {
	/**
	 * A container for buttons that toggles between them when activated.
	 * It works by binding this.toggle at every added element.
	*/

	AbstractGUIElement.call(this, parent, x, y, size_x, size_y);
}
ToggleGroup.prototype = Object.create(AbstractGUIElement.prototype);
ToggleGroup.prototype.constructor = ToggleGroup;
ToggleGroup.prototype.toggle = function() {
	var tmp = this.children;
	for (var c of this.children_hidden) {
		c.show();
	}
	for (var c of tmp) {
		c.hide();
	}
};
ToggleGroup.prototype.add_child = function(object, hidden) {
	AbstractGUIElement.prototype.add_child.call(this, object, hidden);
	object.bind_event('click', this.toggle, this);
};




function Panel(parent, x, y, size_x, size_y) {
	AbstractGUIElement.call(this, parent, x, y, size_x, size_y);
	this.fill = color(255, 128);
	this.stroke = color(0);
	this.stroke_weight = 1;
}
Panel.prototype = Object.create(AbstractGUIElement.prototype);
Panel.prototype.constructor = Panel;
Panel.prototype._draw = function() {
	this.graphics.fill(this.fill);
	this.graphics.stroke(this.stroke);
	this.graphics.strokeWeight(this.stroke_weight);
	this.graphics.rectMode(CORNER);
	this.graphics.rect(this._position_x, this._position_y, this._size_x, this._size_y);
};
