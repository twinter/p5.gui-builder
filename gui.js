/*
 * p5.gui-builder
 * Copyright (C) 2017, see README.md for a list of the contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

'use strict';

function GUI() {
	AbstractGUIElement.call(this, null, 0, 0, windowWidth, windowHeight);
	this.draggable = false;
	this.drag_protected_distance = 5;

	// format of elements in this._touches:
	// [id, x, y, dragging enabled, element, protected]
	this._touches = new Map();

	this._redraw = true;
};
GUI.prototype = Object.create(AbstractGUIElement.prototype);
GUI.prototype.constructor = GUI;
GUI.prototype.draw = function() {
	if (this._redraw) {
		this.graphics.clear();
		AbstractGUIElement.prototype.draw.call(this, 0, 0);
	}
	this._redraw = false;

	image(this.graphics, 0, 0, windowWidth, windowHeight);
};
GUI.prototype.resize = function() {
	this.set_size(windowWidth, windowHeight);
	this.graphics = createGraphics(windowWidth, windowHeight, P2D);

	// traverse the tree of all elements based on root to update graphics
	var todo = this.children.concat(this.children_hidden);
	while (todo.length > 0) {
		// merge the list of children with todo
		var children = todo[0].children.concat(todo[0].children_hidden);
		Array.prototype.push.apply(todo, children);

		// update graphics
		todo[0].graphics = this.graphics;

		// remove the element from the list
		todo.shift();
	}
};
GUI.prototype.touch_start = function() {
	var touch = new Object();

	// determine if it is a mouse or touch event
	if (mouseIsPressed && mouseButton === LEFT) {
		if (this._touches.has(-1) === false) {
			// left mouse button has been clicked
			touch.id = -1;
			touch.x = mouseX;
			touch.y = mouseY;
		}
	}

	if (touch === null) { // event wasn't triggered by a mouse click
		for (let t of touches) {
			if (this._touches.has(t.id) === false) {
				touch.id = t.id;
				touch.x = t.x;
				touch.y = t.y;
			}
		}
	}

	// get the topmost element on the coordinates
	var element = this.check_hit(touch.x, touch.y);

	// let it bubble up until we find an element that defines if it is draggable
	while (element.draggable !== true && element.draggable !== false) {
		element = element.parent;
	}

	// add the element to the list
	touch.draggable = element.draggable;
	touch.element = element;
	touch.protected = true;
	this._touches.set(touch.id, touch);
	touch.element.trigger_event('drag_start');
};
GUI.prototype.touch_move = function() {
	//XXX: check if mouse or touch moved and which one it was

	// check for changes in the known touches
	for (let [k, t] of this._touches) {
		// check if it should be ignored
		if (t.draggable === false && t.protected === false) {
			continue;
		}

		let x, y;

		// check if it moved, next touch if not
		if (t.id === -1) {
			// mouse
			if (t.x === mouseX && t.y === mouseY) {
				continue;
			} else {
				x = mouseX;
				y = mouseY;
			}
		} else {
			// touch
			if (touches[t.id].x === t.x && touches[t.id].y === t.y) {
				continue;
			} else {
				x = touches[t.id].x;
				y = touches[t.id].y;
			}
		}

		// it moved, handle it
		// is it still protected and should it still be?
		if (t.protected) {
			let dx = x - t.x;
			let dy = y - t.y;
			if (this.drag_protected_distance < Math.sqrt(dx * dx + dy * dy)) {
				t.protected = false;
			} else {
				continue;
			}
		}

		// skip the movement
		if (t.draggable === false) {
			continue;
		}

		// move it and trigger event
		let position = t.element.get_position();
		let dx = x - t.x;
		let dy = y - t.y;
		t.x = x;
		t.y = y;
		t.element.set_position(position[0] + dx, position[1] + dy);
		t.element.trigger_event('drag', dx, dy);
	}

};
GUI.prototype.touch_end = function() {
	for (let [id, t] of this._touches) {
		// search for t in touches
		let found = false;

		if (id === -1) {
			// check the mouse
			if (mouseIsPressed && mouseButton === LEFT) {
				continue;
			}
		} else {
			// check the touches
			for (let touch of touches) {
				if (id === touch.id) {
					found = true;
					break;
				}
			}
			if (found) {
				// t has been found, next element in this._touches
				continue;
			}
		}

		// check if it still is protected, trigger click if so
		if (t.protected) {
			this.check_event(t.x, t.y, 'click');
		} else {
			t.element.trigger_event('drag_end');
		}

		this._touches.delete(id);
		return;
	}
};

/*
p5.prototype.createGui = function() {
	window.gui = new GUI();
	return gui;
};
p5.prototype.registerMethod('init', p5.prototype.createGui);  // this may work, i didn't really test it
p5.prototype.registerMethod('resize', p5.prototype.resize);  // doesn't work as is not possible but necessary! how else could it be done?
p5.prototype.registerMethod('post', this.draw);  // this doesn't work as well for some reason

possible solutions for this:
 - use native js events
 - implement an interface in p5
 - use internal variables/methods (this would be a dirty hack as it uses undocumented/internal functions)
*/
